Parbot
======

.. image:: https://img.shields.io/badge/License-AGPL%20v3-blue.svg
    :target: https://www.gnu.org/licenses/agpl-3.0
    :alt: License: AGPL v3

A bot that posts events from a given online calendar to Mastodon.

Install
-------

Install as follow::

    pip3 install --user git+https://framagit.org/HgO/parbot.git

    # download the config file template
    curl -L -o config.ini "https://framagit.org/HgO/parbot/raw/master/config.ini"

    # then tweak the config file (see below)

Usage
-----

Calendar import
^^^^^^^^^^^^^^^

``parbot-agenda`` will import events from the calendar defined in your
``config.ini`` file (see below for configuration details).

It is intended to be used as a cronjob. For instance::

   @hourly CONFIG_FILE=/path/to/your/config/file.ini /home/youruser/.local/bin/parbot-agenda > /dev/null

Reminders
^^^^^^^^^

``parbot`` will wait for requests from Mastodon.

When an user send ``!reminder`` in reply to an event tooted by the bot,
this program will schedule a reminder one day before the event.
It is also possible to specify the time between the reminder and the event.
For instance, ``!reminder 1w`` will schedule a reminder one week before the event.

The users can cancel their reminders with the ``!cancel`` command.
They can also get some help message with ``!help``.

Finally, the bot will boost the events one day before the event date.

**Tips:** If you plan to run parbot on a server, you can use ``nohup`` to avoid the program to be killed after logging out::

    nohup parbot &

Configuration file
^^^^^^^^^^^^^^^^^^

All ``parbot`` scripts will look for a ``config.ini`` file in the current directory,
unless you set the ``CONFIG_FILE`` environment variable to a valid path::

    CONFIG_FILE=/etc/parbot.ini parbot-agenda
    CONFIG_FILE=/etc/parbot.ini parbot

    # or via export
    export CONFIG_FILE=/etc/parbot.ini
    parbot-agenda
    parbot

Configuration
-------------

Calendar
^^^^^^^^

In the ``calendar`` section, you can choose the class that will be used for importing the events with the ``class`` parameter.

At the moment, available classes are:

* Wiki : imports events from the `Pirate Party wiki <https://wiki.pirateparty.be/Calendar>`_
* NextCloud : imports events from a NextCloud server. Calendars can be either public or private.
* CalDav : imports events from a CalDav server
* ICalendar : imports events from a .ics on a server.

In case you must authenticate to the server (e.g. NextCloud or another CalDav server),
you can indicate your credentials with ``user`` and ``password`` of the config file.

Mastodon
^^^^^^^^

You will need to provide an access token in order to connect to Mastodon.
In the ``mastodon`` section, you can indicate the file containing that token with ``access_token``.
If such a file doesn't exist, then the program will ask you your credentials to register the app to Mastodon.
This operation won't store your credentials, as this is critical information.
Instead, it will generate an access token and store it in the file specified in ``access_token``.

It is possible to change the visibility of the toots. By default, the events are tooted in public mode.
For instance, ``visibility = unlisted`` would avoid spamming the global timeline.
However, due to the behaviour of Mastodon, the events wouldn't appear in the tags timelines anymore.

Development
-----------

To have a working copy of the project, simply run the following::

    git clone https://framagit.org/HgO/parbot.git
    pip3 install --user -e .
    # alternatively, run `pip3 install -e .` within a virtualenvironment

Then start hacking!
