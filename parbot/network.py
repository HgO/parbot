import logging
import os
import sys
import time
from getpass import getpass

import i18n
from mastodon import Mastodon, \
    MastodonNetworkError, MastodonNotFoundError, \
    MastodonIllegalArgumentError, MastodonUnauthorizedError

import parbot.config

def register(token_file):
    mastodon_config = parbot.config.parser['mastodon']
    
    print(i18n.t('mastodon.register.message').format(filename=token_file))
    
    answer = None
    while answer not in [ 'y', 'n', 'yes', 'no' ]:
        try:
            answer = input(i18n.t('mastodon.register.confirm').format(
                instance=mastodon_config['instance']) + ' ')
        except KeyboardInterrupt:
            print()
            sys.exit(1)
        answer = 'y' if not answer else answer.lower()
        
    if not answer.startswith('y'):
        sys.exit(1)
    
    scopes = [ 
        'read:accounts', 
        'read:notifications', 
        'read:statuses',
        'write:conversations',
        'write:statuses'
    ]
    
    try:
        client_id, client_secret = Mastodon.create_app('Parbot',
            scopes=scopes,
            api_base_url=mastodon_config['instance'])
    except MastodonNetworkError as e:
        print(i18n.t("mastodon.connection.error").format(
            instance=mastodon_config['instance']), file=sys.stderr)
        print(str(e), file=sys.stderr)
        sys.exit(1)
        
    mastodon = Mastodon(client_id, client_secret,
        api_base_url=mastodon_config['instance'])
    
    logged = False
    while not logged:
        try:
            mastodon.log_in(
                input(i18n.t('mastodon.email') + ' '), 
                getpass(i18n.t('mastodon.password') + ' '),
                scopes=scopes,
                to_file=token_file)
            logged = True
        except MastodonIllegalArgumentError as e:
            print(i18n.t('mastodon.register.invalid_credentials'), 
                file=sys.stderr)
        except KeyboardInterrupt:
            print()
            sys.exit(1)
    
    return mastodon

def connect():
    mastodon_config = parbot.config.parser['mastodon']
    
    token_file = mastodon_config['access_token']
    
    try:
        mastodon = Mastodon(
            access_token=token_file,
            api_base_url=mastodon_config['instance']
        )
    except MastodonNetworkError as e:
        print(i18n.t("mastodon.connection.error").format(
            instance=mastodon_config['instance']), file=sys.stderr)
        print(str(e), file=sys.stderr)
        sys.exit(1)
    
    try:
        mastodon.user = mastodon.account_verify_credentials()
    except MastodonUnauthorizedError:
        mastodon = register(token_file)
        mastodon.user = mastodon.account_verify_credentials()
        
        print(i18n.t("mastodon.register.success"))
    
    return mastodon

def stream(listener):
    min_duration = 5
    
    start_time = time.time()
    end_time = start_time + min_duration
    
    stop = False
    while not stop and end_time - start_time >= min_duration:
        logging.debug('Starting Mastodon stream listener')
        
        start_time = time.time()
        
        try:
            mastodon.stream_user(listener)
        except KeyboardInterrupt:
            stop = True
        except MastodonNetworkError as e:
            logging.critical("Aborting stream listener…", exc_info=e)
        
        end_time = time.time()
        
        duration = end_time-start_time
        logging.debug(f'Execution time : {duration}s')

mastodon = connect()
