CREATE TABLE IF NOT EXISTS `cache` (
    id VARCHAR(255) PRIMARY KEY NOT NULL,
    url TEXT NOT NULL,
    title VARCHAR(255) NOT NULL,
    start_date UNSIGNED BIGINT,
    end_date UNSIGNED BIGINT,
    location VARCHAR(255),
    icon VARCHAR(25),
    toot UNSIGNED BIGINT
);

CREATE TABLE IF NOT EXISTS `cache_workgroups` (
    event VARCHAR(255) NOT NULL 
        REFERENCES cache(id) 
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    workgroup VARCHAR(255) NOT NULL,
    url TEXT NOT NULL,
    CONSTRAINT key_workgroup PRIMARY KEY (event, workgroup)
);

CREATE TABLE IF NOT EXISTS `cache_topics` (
    event VARCHAR(255) NOT NULL 
        REFERENCES cache(id) 
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    topic VARCHAR(255) NOT NULL,
    url TEXT NOT NULL,
    CONSTRAINT key_topic PRIMARY KEY (event, topic)
);

CREATE TABLE IF NOT EXISTS `toots` (
    toot UNSIGNED BIGINT PRIMARY KEY NOT NULL,
    event VARCHAR(255) NOT NULL 
        REFERENCES cache(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    date UNSIGNED BIGINT NOT NULL,
    boosted BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS `reminders` (
    account BIGINT NOT NULL,
    event VARCHAR(255) NOT NULL 
        REFERENCES cache(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    toot UNSIGNED BIGINT NOT NULL,
    date UNSIGNED BIGINT NOT NULL,
    visibility VARCHAR(10) 
        CHECK( visibility IN ('direct', 'private', 'unlisted', 'public') )
        NOT NULL,
    spoiler_text TEXT,
    CONSTRAINT key_reminder PRIMARY KEY (account, event)
);
