import calendar
import re
from datetime import date, timedelta

RE_DURATION = re.compile(
    r'(\d+) *(y(?:ears?)?'
    r'|m(?:onths?)?'
    r'|w(?:eeks?)?'
    r'|d(?:ays?)?'
    r'|h(?:ours?)?'
    r'|min(?:utes?)?)'
    r'(?=[^a-z]|$)',
    re.IGNORECASE)

def parse_duration(text):
    total_duration = timedelta(0)
    
    for match in RE_DURATION.finditer(text):
        duration = int(match.group(1))
        units = match.group(2).lower()
        
        total_duration += make_duration(duration, units)
        
    return total_duration

def make_duration(duration = None, units = None):
    """Creates a duration object from a number and units.
    
    Supported units are: minute, hour, day, week, month and year.
    """
    
    if duration is None:
        duration = 1
    
    if units is None:
        units = 'd'
    
    if units.startswith('min'):
        return timedelta(0, 60 * duration)
    
    if units.startswith('y'):
        return timedelta(365 * duration)
    
    if units.startswith('m'):
        current_month = date.today().month
        
        months = calendar.mdays[current_month:current_month+duration]
        days = sum(months)
        
        return timedelta(days)
    
    if units.startswith('w'):
        return timedelta(7 * duration)
    
    if units.startswith('h'):
        return timedelta(0, 3600 * duration)
    
    # default to day units
    return timedelta(duration)
