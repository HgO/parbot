import logging
import re
from datetime import datetime

from mastodon import StreamListener, MastodonNetworkError
import i18n

import parbot.utils.date
import parbot.model
import parbot.network
import parbot.schedule
import parbot.periodic

class ReminderListener(StreamListener):
    RE_PATPAT = re.compile(r':blobpats:')
    RE_HELP = re.compile(
        r'{regex}'.format(regex=i18n.t('command.help.regex')), 
        re.IGNORECASE)
    RE_REMINDER = re.compile(
        r'{regex}\s+'.format(regex=i18n.t('command.reminder.regex'))
        + parbot.utils.date.RE_DURATION.pattern,
        re.IGNORECASE)
    RE_CANCEL = re.compile(
        r'{regex}'.format(regex=i18n.t('command.cancel.regex')), 
        re.IGNORECASE)

    def __init__(self, scheduler):
        self.db = parbot.model.connect()

        self.cache = parbot.model.table.Cache(self.db)

        self.scheduler = scheduler

        self.commands = (
            (self.RE_HELP, self.cmd_help),
            (self.RE_REMINDER, self.cmd_remind),
            (self.RE_CANCEL, self.cmd_cancel),
            (self.RE_PATPAT, self.cmd_patpat)
        )

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self.db.close()

    def on_notification(self, notification):
        if notification.type != 'mention':
            return

        toot = notification.status

        for regex, cmd in self.commands:
            match = regex.search(toot.content)

            if not match:
                continue

            args = match.groups()

            try:
                return cmd(toot, *args)
            except Exception as e:
                logging.critical(f"{toot.url} - "
                    f"An error occurred, notification couldn't be processed",
                    exc_info=e)
                return

        logging.warn(f"{toot.url} - Cannot parse the command")

        self._send_reply(toot, i18n.t('command.unknown.message'))

    def cmd_help(self, toot):
        logging.info(f"{toot.url} - Showing help")

        self._send_reply(toot, i18n.t('command.help.message'))

    def cmd_remind(self, toot, duration = None, duration_units = None):
        event = self._find_event(toot)

        if event is None:
            logging.warn(f"{toot.url} - Cannot find the event to remind")

            self._send_reply(toot, i18n.t('command.reminder.event_not_found'))
            return

        if not event.start_date:
           logging.warn(f"{toot.url} - "
                f"Cannot schedule a reminder for an event with no date: "
                f"{event.id}")

           self._send_reply(toot, i18n.t('command.reminder.event_without_date'))
           return

        if event.start_date < datetime.now():
            date = parbot.model.dateformat(event.start_date)
            logging.warn(f"{toot.url} - "
                f"Cannot schedule a reminder for past event {event.id}"
                f"({date})")

            self._send_reply(toot, i18n.t('command.reminder.past_event'))
            return
        
        duration = parbot.utils.date.parse_duration(toot.content)

        reminder = parbot.model.data.Reminder(toot, event, duration)
        reminder_date = parbot.model.dateformat(reminder.date)
        
        if reminder.date < datetime.now():
            logging.warn(f"{toot.url} - Cannot schedule a reminder in the past for {event.id} on {reminder_date}")

            self._send_reply(toot, i18n.t('command.reminder.past_reminder'))

            return

        self.scheduler.schedule(reminder)

        self._send_reply(toot, i18n.t('command.reminder.message').format(
                event=event.title if event.title else i18n.t('event.this'),
                date=reminder_date))

    def cmd_cancel(self, toot):
        event = self._find_event(toot)

        if event is None:
            logging.warn(f"{toot.url} - Cannot find the event to cancel")

            self._send_reply(toot, i18n.t('command.cancel.event_not_found'))
            return

        title_event = event.title if event.title else i18n.t('event.this')

        reminder = parbot.model.data.Reminder(toot, event)
        if self.scheduler.cancel(reminder):
            self._send_reply(toot, i18n.t('command.cancel.message').format(
                event=title_event))
        else:
            self._send_reply(toot, i18n.t('command.cancel.reminder_not_found').format(
                event=title_event))

    def cmd_patpat(self, toot):
        logging.info(f"{toot.url} - patpat")

        self._send_reply(toot, ':blobmiou:')

    def _find_event(self, toot):
        reply_id = toot.in_reply_to_id
        if reply_id is None:
            logging.warn(f"{toot.url} - Command was not in reply to an event")

            self._send_reply(toot, i18n.t('command.unknown.not_in_reply'))
            return

        context = parbot.network.mastodon.status_context(reply_id)
        parent_ids = (reply_id,) \
            + tuple(toot.id for toot in context.ancestors)

        return self.cache.find_event(parent_ids)

    def _send_reply(self, toot, message, untag = True):
        reply = parbot.model.data.Toot.make_reply(toot, message,
            untag=untag)

        try:
            reply.send()
        except MastodonNetworkError as e:
            logging.critical(f"{toot.url} - "
                f"Cannot send reply ({reply.id}) due to network issues",
                exc_info=e)


def main():
    with parbot.periodic.PeriodicBoost():
        with parbot.schedule.Scheduler() as scheduler:
            with ReminderListener(scheduler) as listener:
                parbot.network.stream(listener)

    logging.info("Shutting down…")

if __name__ == '__main__':
   main()
