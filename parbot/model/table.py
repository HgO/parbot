import contextlib
import sqlite3
import time
from datetime import datetime

from mastodon import MastodonError

import parbot.config
import parbot.network
from .data import Event, InReplyToot, Reminder, Page


def connect(load_schemas = True, **flags):
    """Establishes a connection to the sqlite3 database and loads the 
    initialization schemas when load_schemas is True.
    """
    
    database_config = parbot.config.parser['sqlite3']
    
    db = sqlite3.connect(database_config['database'], **flags)
    db.execute("PRAGMA foreign_keys = ON")
    
    if not load_schemas:
        return db
    
    with open(database_config['schemas'], 'r') as ifs:
        with contextlib.closing(db.cursor()) as cursor:
            cursor.executescript(ifs.read())
    
    return db

class Table:
    def __init__(self, db):
        self.db = db

class Toots(Table):
    """This table stores the event toot fetched from the wiki and posted on
    Mastodon."""
    
    def __contains__(self, toot_ids):
        if isinstance(toot_ids, int):
            toot_ids = (toot_ids,)
        
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT COUNT(1) FROM `toots` "
                "WHERE toot IN ({toots})".format(
                    toots=','.join('?' for _ in toot_ids)
                ), toot_ids)
            
            count, = cursor.fetchone()
        
        return count == len(toot_ids)
    
    def toot(self, toot_id):
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT date, boosted FROM `toots` "
                "WHERE toot = :toot", { 'toot' : toot_id })
            
            row = cursor.fetchone()
        
        if row is None:
            return row
    
        timestamp, boosted = row
        
        date = datetime.fromtimestamp(timestamp)
        boosted = bool(boosted)
        
        return date, boosted
    
    def boost(self, toot_id):
        with self.db as cursor:
            cursor.execute("UPDATE `toots` SET boosted = 1 "
                "WHERE toot = :toot", { 'toot' : toot_id })
    
    def add(self, toot, event):
        with self.db as cursor:
            cursor.execute("INSERT INTO `toots` (toot, event, date, boosted) "
            "VALUES ("
                ":toot,"
                ":event,"
                ":date,"
                ":boosted"
            ")", { 
                'toot' : toot.id, 
                'event' : event.id, 
                'date' : toot.created_at.timestamp(),
                'boosted' : toot.reblogged
            })

class Reminders(Table):
    """This table stores the reminders scheduled by the users"""
    
    def add(self, reminder):
        with self.db as cursor:
            cursor.execute("REPLACE INTO `reminders` ("
                "account,"
                "event,"
                "toot,"
                "date,"
                "visibility,"
                "spoiler_text"
            ") VALUES ("
                ":account,"
                ":event,"
                ":toot,"
                ":date,"
                ":visibility,"
                ":spoiler_text"
            ")", { 
                'account': reminder.account.id,
                'event' : reminder.event.id,
                'toot' : reminder.toot.id,
                'date' : reminder.date.timestamp(),
                'visibility' : reminder.toot.visibility,
                'spoiler_text' : reminder.toot.spoiler_text
            })
            
    def remove(self, reminder):
        with self.db as cursor:
            cursor.execute("DELETE FROM `reminders` "
                "WHERE account = :account AND event = :event",
                { 'account' : reminder.account.id, 'event' : reminder.event.id })
            
    def reminders(self):
        toots = {}
        events = {}
        
        cache = Cache(self.db)
        
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT account, event, toot, date, visibility, spoiler_text FROM `reminders`")
            
            for account_id, event_id, toot_id, timestamp, visibility, spoiler_text in cursor.fetchall():
                if toot_id not in toots:
                    try:
                        toots[toot_id] = parbot.network.mastodon.status(toot_id)
                    except MastodonError:
                        try:
                            account = parbot.network.mastodon.account(account_id)
                        except MastodonError:
                            continue
                        
                        toots[toot_id] = InReplyToot(toot_id, account, visibility, spoiler_text)
                
                toot = toots[toot_id]
                
                if event_id not in events:
                    events[event_id] = cache.event(event_id)
                event = events[event_id]
                
                date = datetime.fromtimestamp(timestamp)
                
                yield Reminder(toot, event, date=date)

class Pages(Table):
    """This table stores the page related to each event."""
    
    def pages(self, event):
        with contextlib.closing(self.db.cursor()) as cursor:
            name = self._FIELD_NAME
            table = self._TABLE
            
            cursor.execute(f"SELECT url, {name} FROM `{table}` "
                "WHERE event = :event", { 'event' : event.id })
            
            for url, name in cursor.fetchall():
                yield Page(url, name)
                
    def __contains__(self, name):
        with contextlib.closing(self.db.cursor()) as cursor:
            table = self._TABLE
            
            cursor.execute(f"SELECT 1 FROM `{table}` WHERE name = :name", 
                { 'name' : name })
            
            return cursor.fetchone() is not None
        
    def is_same(self, event, pages):
        pages_db = tuple(self.pages(event))
        
        if len(pages_db) != len(pages):
            return False
                
        for page_db in pages_db:
            if page_db not in pages:
                return False
            
        return True
        
    def clear(self, event):
        with contextlib.closing(self.db.cursor()) as cursor:
            table = self._TABLE
            cursor.execute(f"DELETE FROM `{table}` WHERE event = :event", 
                { 'event' : event.id })
            
    def update(self, event, pages):
        with contextlib.closing(self.db.cursor()) as cursor:
            table = self._TABLE
            name = self._FIELD_NAME
                
            cursor.executemany(f"INSERT INTO `{table}` (event, {name}, url) "
                "VALUES (:event, :name, :url)", 
                ({ 'event' : event.id, 'name' : page.name, 'url' : page.url }
                    for page in pages))
             
        
class Workgroups(Pages):
    """This table stores which workgroups organize each event"""
    
    _FIELD_NAME = 'workgroup'
    _TABLE = 'cache_workgroups'
    
    def is_same(self, event):
        return super().is_same(event, set(event.workgroups))
    
    def update(self, event):
        super().update(event, event.workgroups)
    
class Topics(Pages):
    """This table stores which topics are relate to each event"""
    
    _FIELD_NAME = 'topic'
    _TABLE = 'cache_topics'
    
    def is_same(self, event):
        return super().is_same(event, set(event.topics))

    def update(self, event):
        super().update(event, event.topics)

class Cache(Table):
    """This table store each event fetched from the wiki."""
    
    def __init__(self, db):
        super().__init__(db)
        
        self.workgroups = Workgroups(db)
        self.topics = Topics(db)
        
        self.toots = Toots(db)
    
    def is_same(self, event):
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT 1 FROM `cache` "
                "WHERE id = :id AND url = :url AND title = :title "
                "AND start_date = :start_date AND end_date = :end_date "
                "AND location = :location AND icon = :icon", {
                'id' : event.id,
                'url' : event.url,
                'title' : event.title,
                'start_date' : event.start_date.timestamp(),
                'end_date': event.end_date.timestamp(),
                'location' : event.location,
                'icon' : event.icon
            })
            row = cursor.fetchone()
            
        if row is None:
            return False
        
        if not self.workgroups.is_same(event):
            return False
        
        if not self.topics.is_same(event):
            return False
            
        return True
    
    def __contains__(self, event_id):
        if isinstance(event_id, Event):
            event_id = event_id.id
        
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT 1 FROM `cache` WHERE id = :id", 
                { 'id' : event_id })
            
            return cursor.fetchone() is not None
    
    def future_events(self):
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT "
                "id,"
                "url,"
                "title,"
                "start_date,"
                "end_date,"
                "location,"
                "icon,"
                "toot "
            "FROM `cache` WHERE start_date > :now", { 'now' : time.time() })
            
            rows = cursor.fetchall()
            
        for row in rows:
            yield self._create_event(*row)
            
    def event(self, event_id):
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT "
                "url,"
                "title,"
                "start_date,"
                "end_date,"
                "location,"
                "icon "
            "FROM `cache` WHERE id = :id", { 'id' : event_id })
            
            row = cursor.fetchone()
            if row is None:
                return row
            
        event = self._create_event(event_id, *row)
        
        workgroups = list(self.workgroups.pages(event))
        event.workgroups = workgroups
        
        topics = list(self.topics.pages(event))
        event.topics = topics
        
        return event
    
    def find_event(self, toot_ids):
        if isinstance(toot_ids, int):
            toot_ids = (toot_ids,)
        
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT "
                "id,"
                "url,"
                "title,"
                "start_date,"
                "end_date,"
                "location,"
                "icon,"
                "toot "
                "FROM `cache` WHERE toot IN ({toots})".format(
                    toots=','.join('?' for _ in toot_ids)
                ), toot_ids)
            
            row = cursor.fetchone()
            if row is None:
                return row
        
        return self._create_event(*row)
            
    def add(self, event):
        with self.db as cursor:
            cursor.execute("REPLACE INTO `cache` ("
                "id,"
                "url,"
                "title,"
                "start_date,"
                "end_date,"
                "location,"
                "icon,"
                "toot"
            ") VALUES ("
                ":id,"
                ":url,"
                ":title,"
                ":start_date," 
                ":end_date,"
                ":location,"
                ":icon,"
                ":toot"
            ")", {
                'id' : event.id,
                'url' : event.url,
                'title' : event.title,
                'start_date' : event.start_date.timestamp(),
                'end_date' : event.end_date.timestamp(),
                'location' : event.location,
                'icon' : event.icon,
                'toot' : event.toot.id 
                    if event.toot is not None else event.toot
            })
            
            self.workgroups.clear(event)
            self.workgroups.update(event)
            
            self.topics.clear(event)
            self.topics.update(event)
            
            if event.toot is not None:
                self.toots.add(event.toot, event)
    
    def move(self, from_id, to_id, url):
        with self.db as cursor:
            cursor.execute("UPDATE `cache` SET id = :new_id, url = :url "
                "WHERE id = :old_id", 
                { 'old_id': from_id, 'new_id': to_id, 'url' : url })
    
    @staticmethod
    def _create_event(id, url, title, start_date, end_date, location, icon,
            toot = None):
        if start_date:
            start_date = datetime.fromtimestamp(start_date)
        
        if end_date:
            end_date = datetime.fromtimestamp(end_date)
            
        return Event(id, url, title, start_date, end_date, location, 
            icon=icon, toot=toot)
