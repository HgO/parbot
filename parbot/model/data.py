import collections
import logging
import uuid
from datetime import datetime, timedelta

import i18n
from mastodon import MastodonNotFoundError

import parbot.config
import parbot.network

def dateformat(date):
    return i18n.t('event.date') \
        .format(date=date, time=timeformat(date)).title()

def timeformat(time):
    return i18n.t('event.time').format(time=time)

class AttrDict(dict):
    def __getattr__(self, attr):
        if attr in self:
            return self[attr]
        else:
            raise AttributeError("Attribute not found: " + str(attr))
    
    def __setattr__(self, attr, val):
        if attr in self:
            raise AttributeError("Attribute-style access is read only")
        
        super(AttrDict, self).__setattr__(attr, val)


class InReplyToot:
    """Wrapper for a toot to which we want to reply, but doesn't exist anymore"""
    def __init__(self, id, account, 
        visibility = None, spoiler_text = None, url = None):
        self.id = id
        
        if url is not None:
            self.url = url
        else:
            self.url = id
        
        self.account = account
        self.visibility = visibility
        self.spoiler_text = spoiler_text
        # we don't store the mentions, as it is not critical
        # moreover, for privacy reasons, it's better to not keep them
        self.mentions = []

class Toot:
    """Wrapper for sending a toot.
    
    The `make_reply` method let you create a reply to a toot. It adds the 
    mentions before the actual message, and it keeps the visibility and spoiler
    text of the toot to which we want to reply. This method is actually a
    wrapper of `Mastodon.status_reply`.
    
    Each `Toot` object has an unique identifier, which is used as an idempotency
    key (to avoid duplicate toot).
    """
    def __init__(self, message, in_reply,
        visibility = None, spoiler_text = None):
        self.id = str(uuid.uuid4())
        
        self.message = message
        self.in_reply = in_reply
        self.visibility = visibility
        self.spoiler_text = spoiler_text
    
    def send(self):
        """Send the reply to mastodon. If the toot to which we want to reply
        doesn't exist anymore, it will fallback to sending a standalone toot 
        as-if it was a reply.
        
        This can raise a MastodonNetworkError in case of network issues.
        
        Returns the toot dict sent.
        """
        try:
            return parbot.network.mastodon.status_post(self.message, 
                in_reply_to_id=self.in_reply.id,
                visibility=self.visibility, 
                spoiler_text=self.spoiler_text,
                idempotency_key=self.id)
        except MastodonNotFoundError:
            logging.warn(f"{self.in_reply.url} - "
                "Cannot send a reply because the toot doesn't exist")
            
            return parbot.network.mastodon.status_post(self.message,
                visibility=self.visibility,
                spoiler_text=self.spoiler_text,
                idempotency_key=self.id)
    
    @staticmethod
    def make_reply(toot, message, *args, **kwargs):
        """Creates a reply to a toot. This method adds the mentions before the 
        actual message, and it keeps the visibility and spoiler text of the 
        toot to which we want to reply. This method is actually a wrapper of 
        `Mastodon.status_reply`.
        """
        
        user_id = parbot.network.mastodon.user.id
        
        untag = kwargs.pop('untag', False)    
        visibility = kwargs.pop('visibility', toot.visibility)
        spoiler_text = kwargs.pop('spoiler_text', toot.spoiler_text)
        
        mentions = collections.OrderedDict()
        
        if toot.account.id != user_id:
            mentions[toot.account.id] = toot.account.acct
            
        if not untag:
            for mention in toot.mentions:
                if mention.id != user_id:
                    mentions[mention.id] = mention.acct
        
        message = ''.join('@' + mention + ' ' for mention in mentions.values()) + message
        
        return Toot(message, toot, 
            visibility=visibility, spoiler_text=spoiler_text)

class Reminder:
    def __init__(self, toot, event, 
        duration = timedelta(1), date = None, account = None):
        self.toot = toot
        self.event = event
        
        if account is not None:
            self.account = account
        else:
            self.account = self.toot.account
            
        if date is not None:
            self.date = date
        else:
            self.date = event.start_date - duration
            
    def __hash__(self):
        return hash((self.account.id, self.event.id))
    
    def __eq__(self, other):
        return self.account.id == other.account \
            and self.event.id == other.event.id
        
    def __neq__(self, other):
        return not (self == other)

class Page:
    def __init__(self, url, name):
        self.url = url
        self.name = name
        
    def __str__(self):
        return self.url
    
    def __hash__(self):
        return hash((self.url, self.name))
    
    def __eq__(self, other):
        return self.url == other.url and self.name == self.name
    
    def __ne__(self, other):
        return not (self == other)
    
class Event:
    def __init__(self, id, url, title,
        start_date = None, end_date = None, location = None, icon = None,
        topics = [], workgroups = [], toot = None):
        self.id = id
        self.url = url
        self.title = title
        
        self.start_date = start_date
        self.end_date = end_date
        
        self.location = location
        self.icon = icon
        
        self.topics = topics
        self.workgroups = workgroups
        
        if isinstance(toot, int):
            self.toot = AttrDict({ 'id' : toot })
        else:
            self.toot = None
        
    def __str__(self):
        event_config = parbot.config.parser['event']
        
        mentions = event_config.get('mentions', '')
        if mentions:
            mentions += ' '
        
        icon = self.icon
        if icon is None:
            icon = event_config.get('icon', '')
        
        if icon:
            icon += ' '
        
        details = [ mentions + icon + self.title, '' ]
        
        if self.start_date:
            details.append(self._datetostr())
            details.append('')
        
        if self.location:
            details.append(self.location)
            details.append('')
        
        if self.url:
            url = self.url
        else:
            url = event_config.get('url', '').format(
                id=self.id,
                title=self.title,
                date=self.start_date)
        
        if url:
            details.append(url)
            details.append('')
        
        if len(self.workgroups):
            details.append(i18n.t('event.organized_by'))
            for workgroup in self.workgroups:
                details.append('- ' + str(workgroup))
            details.append('')
        
        tags = event_config.get('tags', '')
        if len(self.topics):
            if tags:
                tags += ' '
            
            topics = []
            for topic in self.topics:
                tag = '#'
                for part in topic.name.split():
                    tag += part.capitalize()
                
                topics.append(tag)
            
            tags += ' '.join(topics)
        
        if tags:
            details.append(tags)
        
        return '\n'.join(details).rstrip()
    
    def _datetostr(self):
        date_str = dateformat(self.start_date)
        
        if self.end_date != self.start_date:
            date_str += ' - ' + timeformat(self.end_date)
        
        return date_str
    
    def __hash__(self):
        return hash((self.id, self.url, self.title,
            self.start_date, self.end_date, self.location, self.icon))
    
    def __eq__(self, other):
        other_vars = vars(other)
        for var, value in vars(self).items():
            if other_vars[var] != value:
                return False
            
        return True
    
    def __ne__(self, other):
        return not (self == other)
    
    def __lt__(self, other):
        return self.start_date < other.start_date
    
    def __gt__(self, other):
        return not (self < other)
