import logging
import time
import threading
from datetime import datetime, timedelta

from mastodon import MastodonNetworkError, MastodonNotFoundError

import parbot.model
import parbot.network

class PeriodicTask(threading.Thread):
    def __init__(self, interval):
        super().__init__()
        
        self.stopped = threading.Event()
        
        if isinstance(interval, int):
            self.interval = interval
        else:
            self.interval = interval.total_seconds()
    
    def __enter__(self):
        self.start()
        return self
    
    def __exit__(self, *exc):
        self.stop()
    
    def stop(self):
        self.stopped.set()
        self.join()
    
    def run(self):
        task_duration = self.interval
        while not self.stopped.wait(self.interval - task_duration):
            start_time = time.time()
            
            logging.debug("{task} - Executing periodic task".format(
                task=type(self).__name__))
            
            self.task()
            
            task_duration = time.time() - start_time
            
    def task(self):
        raise NotImplementedError

class PeriodicBoost(PeriodicTask):
    def __init__(self, interval = 3600, time_before_boost = timedelta(1)):
        super().__init__(interval)
        
        self.db = parbot.model.connect(check_same_thread=False)
        self.cache = parbot.model.table.Cache(self.db)
        self.toots = parbot.model.table.Toots(self.db)
        
        self.time_before_boost = time_before_boost
    
    def __exit__(self, *exc):
        self.close()
    
    def close(self):
        self.stop()
        self.db.close()
    
    def task(self):
        for event in self.cache.future_events():
            now = datetime.now()
            
            if (event.start_date - now) >= self.time_before_boost:
                continue
            
            date, boosted = self.toots.toot(event.toot.id)
            
            if boosted:
                continue
            
            if (event.start_date - date) < self.time_before_boost:
                continue
            
            try:
                parbot.network.mastodon.status_reblog(event.toot.id)
                
                self.toots.boost(event.toot.id)
                
                logging.info(f"{event.toot.id} - Boosted the event {event.id}")
            except MastodonNotFoundError:
                logging.critical(f"{event.toot.id} - "
                    f"Cannot boost the event {event.id}: toot doesn't exist")
            except MastodonNetworkError as e:
                logging.warn(f"{event.toot.id} - "
                    f"Cannot boost the event {event.id} due to network issues",
                    exc_info=e)
                
