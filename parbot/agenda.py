import json
import logging
import os
import requests
import sys
import urllib.parse
from datetime import date, datetime, timedelta

try:
    import icalendar
    import caldav
except ImportError:
    pass
from mastodon import MastodonError

import parbot.config
import parbot.model
import parbot.network
from parbot.model.data import Page, Event

def load_class(string):
    *module_names, cls_name = string.split('.')

    module = sys.modules[__name__]
    for module_name in module_names:
        module = getattr(module, module_name)
    cls = getattr(module, cls_name)

    return cls

def localize(dt, timezone = None):
    if isinstance(dt, int):
        dt = datetime.fromtimestamp(dt).astimezone(timezone)
        timezone = dt.tzinfo
        
        return dt.replace(tzinfo=None) - timezone.utcoffset(dt)

    if type(dt) is date:
        return datetime.combine(dt, datetime.min.time())
    
    timezone = dt.astimezone(timezone).tzinfo
    return dt.replace(tzinfo=None) + timezone.utcoffset(dt)

class Calendar:
    def __init__(self, url, cache):
        self.url = url
        self.cache = cache

    def query(self):
        return self.url

    def get(self):
        raise NotImplementedError

    def events(self):
        events = map(self.create_event, self.get())
        events = sorted(event for event in events
            if event.start_date and event.start_date > datetime.now())
        
        for event in events:
            if not event.title:
                logging.warn(f"{event.url} - Event has no title")
                continue
            
            if self.event_exists(event):
                continue
            
            yield event
    
    def event_exists(self, event):
        return self.cache.is_same(event)
    
    @staticmethod
    def create_event(item):
        raise NotImplementedError

class Wiki(Calendar):
    def __init__(self, url, cache):
        super().__init__(url, cache)

        now = datetime.now().isoformat() 
        self.SEMANTIC_QUERY = (
            f'[[Has event type::+]] [[Has date::>{now}]]',
            '?Has title=title',
            '?Has date=start_date',
            '?Has end date=end_date',
            '?Has page=page',
            '?In venue=venue',
            '?Has address=address',
            '?In city=city',
            '?In postal code=postal_code',
            '?In topic=topics',
            '?Organized by=workgroups',
            '?Has event type=type',
            'sort=Has date',
            'order=asc',
            'limit=10'
        )

        self.API_QUERY = url + '/api.php?' + urllib.parse.urlencode({
            'action': 'ask',
            'format': 'json',
            'api_version': 3,
            'formatversion': 'latest',
            'utf8': 1,
            'query': '|'.join(self.SEMANTIC_QUERY)
        })

    def query(self):
        return self.API_QUERY

    def _api_request(self, url):
        response = requests.get(url)
        response.raise_for_status()
        
        return json.loads(response.text)

    def get(self):
        try:
            response = self._api_request(self.query())
        except requests.exceptions.HTTPError:
            return
        
        return iter(response['query']['results'])
    
    def event_exists(self, event):
        exists = super().event_exists(event)
        
        if exists:
            return True
        
        query = self.url + '/api.php?' + urllib.parse.urlencode({
            'action': 'query',
            'format': 'json',
            'prop': 'redirects|info',
            'rdprop': 'title',
            'inprop': 'url',
            'titles': event.id
        })
        
        try:
            response = self._api_request(query)
        except requests.exceptions.HTTPError:
            return False
        
        pages = response['query']['pages']
        page = next(iter(pages.values()))
        
        try:
            redirects = page['redirects']
        except KeyError:
            return False
        
        if event in self.cache:
            return False
        
        url = page['fullurl']
        for redirect in redirects:
            redirect_id = redirect['title']
            
            if redirect_id not in self.cache:
                continue
            
            self.cache.move(redirect_id, event.id, url)
            
            return self.cache.is_same(event)
    
    @staticmethod
    def _datefromraw(raw_date):
        _, *date, _ = raw_date.split('/')
        return datetime(*map(int, date))
    
    @staticmethod
    def create_event(page):
        page = next(iter(page.values()))
        details = page['printouts']

        url = details['page'][0]['fullurl']
        id = page['fulltext']

        type = details['type'][0]['fulltext']
        if type == 'PirateDrink':
            icon = '🥂'
        elif type == 'Meeting':
            icon = ':discuss:'
        else:
            icon = '📅'

        try:
            title = details['title'][0]
        except IndexError:
            title = None

        try:
            raw_start_date = details['start_date'][0]['raw']
            start_date = Wiki._datefromraw(raw_start_date)
        except IndexError:
            start_date = None

        try:
            raw_end_date = details['end_date'][0]['raw']
            end_date = Wiki._datefromraw(raw_end_date)
        except IndexError:
            end_date = None
        
        try:
            venue = details['venue'][0]
        except IndexError:
            venue = ''
        
        try:
            address = details['address'][0]
        except IndexError:
            address = ''
        
        try:
            postal_code = str(details['postal_code'][0])
        except IndexError:
            postal_code = ''
            
        try:
            city = details['city'][0]
        except IndexError:
            city = ''
        
        if postal_code and city:
            city = postal_code + ' ' + city
            
        location = [ venue, address, city ]
        location = '\n'.join(item for item in location if item)
        
        if not location:
            location = None

        topics = tuple(Page(topic['fullurl'], topic['fulltext'])
            for topic in details['topics'])
        
        if type != 'External':
            workgroups = tuple(Page(wg['fullurl'], wg['fulltext'])
                for wg in details['workgroups'])
        else:
            workgroups = []

        return Event(id, url, title, start_date, end_date, location, icon, 
            topics=topics, workgroups=workgroups)

class ICalendar(Calendar):
    def query(self):
        return self.url

    def get(self):
        response = requests.get(self.query())

        if response.status_code != 200:
            return

        yield from self._vevents(response.content)

    @staticmethod
    def _vevents(content):
        ical = icalendar.Calendar.from_ical(content)

        for component in ical.walk():
            if component.name == 'VEVENT':
                yield component

    @staticmethod
    def create_event(vevent):
        id = vevent['uid']
        title = vevent.get('summary')
        start_date = localize(vevent['dtstart'].dt)

        event_config = parbot.config.parser['event']
        url = event_config.get('url').format(
            id=id,
            title=title,
            date=start_date
        )

        end_date = vevent.get('dtend')
        if not end_date:
            end_date = start_date
        else:
            end_date = localize(end_date.dt)

        location = vevent.get('location')
        icon = ''

        return Event(id, url, title, start_date, end_date, location, icon)

class CalDav(ICalendar):
    def __init__(self, url, cache):
        super().__init__(url, cache)

        calendar_config = parbot.config.parser['calendar']

        self.client = caldav.DAVClient(url,
            username=calendar_config['user'],
            password=calendar_config['password'])

    def get(self):
        start_date = datetime.now()

        calendar = self.client.principal().calendar(cal_id=self.url)
        content = calendar.date_search(start_date)

        for event_dav in content:
            yield from self._vevents(event_dav.data)

class NextCloud(ICalendar):
    def __init__(self, url, cache, range = None):
        super().__init__(url, cache)

        if range is None:
            range = timedelta(31)
        self._range = range

    def query(self):
        scheme, netloc, path, _, _ = urllib.parse.urlsplit(self.url)

        if '/dav/' not in path:
            path = os.path.dirname(path)
            path = path.replace('index.php', 'remote.php')
            path = path.replace('/apps/calendar/p/', '/dav/public-calendars/')

        if '/dav/public-calendars/' not in path:
            return
        
        start_date = datetime.now()
        end_date = start_date + self._range
        
        query = urllib.parse.urlencode({
            'export': 1,
            'start': int(start_date.timestamp()),
            'end': int(end_date.timestamp()),
            'componentType': 'VEVENT',
            'expand': 1
        })

        return urllib.parse.urlunsplit((scheme, netloc, path, query, ''))

    def get(self):
        query = self.query()

        if not query:
            return CalDav(self.url, self.cache).get()

        return ICalendar(query, self.cache).get()


def main():
    db = parbot.model.connect()
    cache = parbot.model.table.Cache(db)
    
    calendar_cls = load_class(parbot.config.parser['calendar']['class'])
    calendar = calendar_cls(parbot.config.parser['calendar']['url'], cache)
    
    visibility = parbot.config.parser['mastodon']['visibility']
    for event in calendar.events():
        print(str(event))
        try:
            event.toot = parbot.network.mastodon.status_post(str(event),
                visibility=visibility)
        except MastodonError as e:
            logging.error(f"{event.url} - "
                "Cannot post the event to Mastodon", exc_info=e)
            continue

        cache.add(event)

        logging.info(f"{event.toot.url} - Event {event.id} added on Mastodon")

if __name__ == '__main__':
   main()
