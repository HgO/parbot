import logging 
import threading
from datetime import datetime

import i18n
from mastodon import MastodonNetworkError

import parbot.config
import parbot.model

class Scheduler:
    def __init__(self):
        self.db = parbot.model.connect(check_same_thread=False)
        
        self.reminders = parbot.model.table.Reminders(self.db)
        
        self.tasks = {}
        
        self.lock = threading.RLock()
        
        for reminder in self.reminders.reminders():
            self.schedule(reminder)
    
    def __enter__(self):
        return self
    
    def __exit__(self, *exc):
        self.close()
    
    def close(self):
        with self.lock:
            logging.debug("Scheduler - Closing all reminders")
            
            for timer in self.tasks.values():
                timer.cancel()
            self.tasks.clear()
            
            self.db.close()
    
    def schedule(self, reminder):
        toot = reminder.toot
        event = reminder.event
        date = parbot.model.dateformat(reminder.date)
        
        with self.lock:
            if reminder in self.tasks:
                self.cancel(reminder)
                
            if datetime.now() >= event.start_date:
                logging.error(f"{toot.url} - "
                    f"Cannot send reminder for {event.id} before the start of"
                    f"the event ({date})")
                return
        
            if datetime.now() >= reminder.date:
                self.remind(reminder)
                return
            
            timeout = (reminder.date - datetime.now()).total_seconds()
            
            logging.info(f"{toot.url} - "
                f"Scheduling reminder for {event.id} on {date} "
                f"(timeout = {timeout:.0f}s)")
            
            timer = threading.Timer(timeout, lambda: self.remind(reminder))
            timer.start()
            
            self.reminders.add(reminder)
            self.tasks[reminder] = timer
        
        return timer
    
    def remind(self, reminder):
        toot = reminder.toot
        event = reminder.event
        date = parbot.model.dateformat(reminder.date)
        
        with self.lock:
            logging.info(f"{toot.url} - "
                f"Sending reminder for {event.id} on {date}")
            
            reply = parbot.model.data.Toot.make_reply(reminder.toot, 
                i18n.t('reminder.header') + '\n\n' + str(reminder.event),
                untag=False)
            
            sent = False
            max_retries = parbot.config.parser.defaults()['max_retries']
            for retries in range(max_retries):
                if sent or reminder.date >= event.start_date:
                    break
                
                try:
                    reply.send()
                    sent = True
                except MastodonNetworkError as e:
                    logging.error(f"{toot.url} - "
                        f"Cannot send reminder for {event.id} on {date} "
                        "due to network issues", exc_info=e)
            
            if not sent:
                logging.critical(f"{toot.url} - "
                    f"Cannot send reminder for {event.id} on {date} "
                    f"due to network issues after {retries} retries")
            else:
                logging.info(f"{toot.url} - "
                    f"Reminder sent for {event.id} on {date}")
                
            self.reminders.remove(reminder)
            
            if reminder not in self.tasks:
                logging.warn(f"{toot.url} - "
                    f"Reminder for {event.id} on {date} was not in a timer.")
                return
            
            del self.tasks[reminder]
        
    def cancel(self, reminder):
        toot = reminder.toot
        event = reminder.event
        date = parbot.model.dateformat(reminder.date)
        
        with self.lock:
            if reminder not in self.tasks:
                logging.warn(f"{toot.url} - "
                    f"Cannot cancel reminder for {event.id} on {date}")
                return False
            
            logging.info(f"{toot.url} - "
                f"Canceling reminder for {event.id} on {date}")
            
            self.tasks[reminder].cancel()
            del self.tasks[reminder]
            
            self.reminders.remove(reminder)
        
        return True
